import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {Post} from './post';

@Injectable()
export class PostService {

    private apiUrl = 'https://jsonplaceholder.typicode.com/';

    private postsUrl = this.apiUrl + 'posts';  

    constructor(private http: HttpClient) { }

    /** 
    * GET posts from the api 
    */
    getPosts (): Observable<Post[]> {

        return this.http.get<Post[]>(this.postsUrl)
          .pipe(catchError(this.handleError('getPosts', []))
        );
       
    }

    /** 
    * UPDATE: delete the post 
    * @param post - the post to be updated
    */
    updatePost(post: Post | number): Observable<Post> {

        const id = typeof post === 'number' ? post : post.id;

        const url = `${this.postsUrl}/${id}`;

        return this.http.patch<Post>(url, post)
          .pipe(
            catchError(this.handleError<Post>('updatePost'))
          );
      }

    /** 
    * DELETE: delete the post 
    * @param post - the post to be deleted
    */
    deletePost(post: Post | number): Observable<Post> {

        const id = typeof post === 'number' ? post : post.id;

        const url = `${this.postsUrl}/${id}`;

        return this.http.delete<Post>(url)
          .pipe(
            catchError(this.handleError<Post>('deletePost'))
          );
      }

    /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {

        console.error(error);

        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }


}


