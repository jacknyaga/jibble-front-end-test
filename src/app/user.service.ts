import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {User} from './user';

@Injectable()
export class UserService {

    private apiUrl = 'https://jsonplaceholder.typicode.com/';

    private usersUrl = this.apiUrl + 'users';  

    constructor(private http: HttpClient) { }

     /** 
    * GET posts from the api 
    */
    getUsers (): Observable<User[]> {

        return this.http.get<User[]>(this.usersUrl)
            .pipe(catchError(this.handleError('getUsers', []))
        );
       
    }

    /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {

        console.error(error);

        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }


}


