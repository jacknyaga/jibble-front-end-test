import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import {Album} from './album';

@Injectable()
export class AlbumService {

    private apiUrl = 'https://jsonplaceholder.typicode.com/';

    private albumsUrl = this.apiUrl + 'albums';  

    constructor(private http: HttpClient) { }

    /** 
    * GET albums from the api 
    */
    getAlbums (): Observable<Album[]> {

        return this.http.get<Album[]>(this.albumsUrl)
          .pipe(catchError(this.handleError('getAlbums', []))
        );
       
    }

    /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {

        console.error(error);

        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }


}