import {User} from './user';
import {Post} from './post';
import {Album} from './album';


export class Collection {
    post : Post;

    album : Album;

    user : User;
}
