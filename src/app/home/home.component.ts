import { Component, OnInit, Input } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import "rxjs/add/observable/zip";

import {UserService} from '../user.service';
import {User} from '../user';

import {PostService} from '../post.service';
import {Post} from '../post';

import {AlbumService} from '../album.service';
import {Album} from '../album';

import {Collection} from '../collection';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    @Input() collection : Collection;

    users : User[];
    posts : Post[] = [];
    albums : Album[] = [];
    collections : Collection[] = [];

    constructor(private UserService : UserService, private PostService : PostService, private AlbumService : AlbumService) { }

    ngOnInit() {

        let usersData = this.UserService.getUsers();
        let postsData = this.PostService.getPosts();
        let albumData = this.AlbumService.getAlbums();

        Observable.zip(
            postsData,
            albumData,
            usersData,
            (post : Post, album : Album, user : User) => ({post, album, user})
         )
        .subscribe(zippedData => {
             this.buildCollection(zippedData)
         })

    }

    /**
    * Build the collection from the zipped values from the observables
    *@param zippedData - the emitted values
    */
    buildCollection(zippedData) {
        zippedData.post.forEach(function (item,  index) {

            if (index < 30) {
                const citem = new Collection;

                citem.post = item;
                citem.user = zippedData.user.filter(user => user.id == item.userId)[0];
                citem.album = zippedData.album[index];

                this.collections.push(citem);
            }

        }, this);
    }

    /**
    * Update the collection and the child post
    *@param collection - the collection to be updated
    */
    update(collection): void {
       this.PostService.updatePost(collection.post)
         .subscribe();
    }

    /**
    * Delete the collection and the child post
    *@param collection - the collection to be deleted
    */
    delete(collection: Collection): void {
      this.PostService.deletePost(collection.post.id).subscribe();
      this.collections = this.collections.filter(c => c !== collection);
    }

}