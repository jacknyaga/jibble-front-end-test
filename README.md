## Jibble Front-end Test App
#### Test app project for Jibble front-end position


----------


This is a simple CRUD application using the JSONPlaceholder API at https://jsonplaceholder.typicode.com/. The App does the following:

 1. Fetches 3 resources (posts, users, albums)  
 2. Sorts them then combines them into a collection
 3. Displays them as a list on the main page


##Prerequisites

----------

This app is built on Angular2 and Angular-cli which have dependencies that require Node 6.9.0 or higher, together with NPM 3 or higher.

###Install npm packages
`npm install`

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build the app for deployment

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.
